-- chikun :: 2014-2015
-- Template state


-- Temporary state, removed at end of script
local TemplateState = class(PrototypeClass, function(new_class) end)


-- On state create
function TemplateState:create()

	self.rotation = 0

	cm.current = maps.test_map

	cb.test_bg:play()
end


-- On state update
function TemplateState:update(dt)

	self.rotation = self.rotation + dt
end


-- On state draw
function TemplateState:draw()

	lg.setColor(255, 255, 255)

	lg.print(lt.getFPS(), 480, 32)

	cg.drawCentred(gfx.sexy_halloween, 256, 256, self.rotation, 0.8)

	lg.translate(768, 128)

		cm.draw()
	lg.origin()
end


-- On state kill
function TemplateState:kill()

	self.x = nil
end


-- Transfer data to state loading script
return TemplateState
